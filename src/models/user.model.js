const { Schema, model } = require('mongoose');

const UserSchema = Schema({
    name: {
        type: String,
        required: [true, 'This name is required']
    },
    email: {
        type: String,
        required: [true, 'This email is required'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'This password is required'],
    },
    img: {
        type: String
    },
    // rol: {
    //     type: String,
    //     required: true,
    //     emun: ['Admin_Role, User_Role']
    // },
    // status: {
    //     type: Boolean,
    //     default: true
    // },
    // google: {
    //     type: Boolean,
    //     default: false

    // }

});

UserSchema.methods.toJSON = function() {
    const { __v, password, _id, ...user } = this.toObject();
    user.uid = _id;
    return user;
}

module.exports = model('User', UserSchema);
