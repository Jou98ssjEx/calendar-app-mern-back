// const CategoryModel = require('./category.model');
// const ProductModel = require('./product.model');
// const RolModel = require('./rol.model');
const UserModel = require('./user.model');
const EventModel = require('./event.model');

module.exports = {
    // CategoryModel,
    // ProductModel,
    // RolModel,
    EventModel,
    UserModel
}
