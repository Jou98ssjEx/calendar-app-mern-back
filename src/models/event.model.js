const { Schema, model } = require('mongoose');

const EventSchema = Schema({
    title:{
        type: String,
        required: [true, 'This title is required'],
    },
    notes:{
        type: String,
    },
    start:{
        type: Date,
        required: [true, 'This date inital is required'],
    },
    end:{
        type: Date,
        required: [true, 'This date end is required'],
    },
    user:{
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'This user end is required'],
    }

})

EventSchema.methods.toJSON = function() {
    const { __v, _id, ...rest } = this.toObject();
    rest.id = _id;
    return rest;
}

module.exports = model('Event', EventSchema);