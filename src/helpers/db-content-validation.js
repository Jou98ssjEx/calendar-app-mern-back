const { EventModel } = require("../models");

const existEventById = async ( id ) =>{
    const eventExist = await EventModel.findById(id);
    if (!eventExist) {
        throw new Error(`El ${id}, no le pertence a ningun evento`);
    }

}

module.exports ={
    existEventById,
}