const jwt = require('jsonwebtoken');

const { UserModel } = require('../models');

const generateToken = (uid = '') => {

    return new Promise((resolve, reject) => {

        const payload = { uid };

        jwt.sign(payload, process.env.SECRETORPRIVATEKEY, {
            expiresIn: '4h'
        }, (error, token) => {
            if (error) {
                console.log(error);
                reject('Error en el token');
            } else {
                resolve(token);
            }
        });

    });
}

const checkToken = async(token = '') => {

    try {


        if (token.length < 10) {
            return null
        }

        // Verificar el token
        const { uid } = jwt.verify(token, process.env.SECRETORPRIVATEKEY);

        const user = await UserModel.findById(uid);

        if (user) {
            if (user.status) {
                return user
            } else {
                return null
            }
        } else return null

    } catch (error) {

        console.log(error);
    }
}

module.exports = {
    generateToken,
    checkToken,
}
