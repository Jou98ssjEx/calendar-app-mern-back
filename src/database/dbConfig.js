const mongoose = require('mongoose');
 require('colors');

const dbConfig = async ( ) => {
    const dbProd = 'Prod';
    try {
       await mongoose.connect(process.env.MONGO_DB);
       console.log(`DataBase Online in: ${ dbProd.green} `)
    } catch (error) {
        console.log(error);
        throw new Error('Error connecting the database');
    }
}

module.exports = dbConfig;