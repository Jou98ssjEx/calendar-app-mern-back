const { request, response } = require("express");
const jwt = require('jsonwebtoken');

const { UserModel } = require('../models');


const validateJwt = async(req = request, res = response, next) => {

    const token = req.header('x-token');
    // console.log(token);
    if (!token) {
        return res.status(401).json({
            ok: false,
            msg: 'No token exists'
        });
    }

    try {

        const { uid } = jwt.verify(token, process.env.SECRETORPRIVATEKEY);

        // leer usuario Auth
        // read user Auth
        const userAuth = await UserModel.findById(uid);

        // Validar si el usuario existe
        // Validate if the user exists
        if (!userAuth) {
            return res.status(401).json({
                msg: `Token not valid - user doesn't exist`
            })
        }

        // Validar el estado del usuario
        // Validate user status
        // if (!userAuth.status) {
        //     return res.status(401).json({
        //         msg: `Token not valid - user inactive`
        //     })
        // }

        req.userAuth = userAuth;

        req.uid = uid;

        next();

    } catch (error) {
        console.log(error);
        res.status(401).json({
            ok: false,
            msg: 'Error sending token',
            error
        });
    }
}

module.exports = {
    validateJwt
}
