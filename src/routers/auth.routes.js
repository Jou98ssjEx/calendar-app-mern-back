const { Router } = require('express');
const { check } = require('express-validator');
const { createUser, loginUser, renewToken } = require('../controllers/auth.controlllers');
const validateContent = require('../middlewares/validate-content');
const { validateJwt } = require('../middlewares/validate-jwt');

const route = Router();

route.post('/new', [
    check('name', 'Name is required').not().isEmpty(),
    check('email', 'Email is required').isEmail(),
    check('password', 'Password min 6 caracters').isLength({min: 6}),
    validateContent,
], createUser );

route.post('/login', [
    check('email', 'Email is required').isEmail(),
    check('password', 'Password is required').not().isEmpty(),
    validateContent,
], loginUser );

route.get('/renew',validateJwt, renewToken );

module.exports = route;