const { Router } = require('express');
const { check } = require('express-validator');
const { createEvent, updateEvent, deleteEvent, getEvents, getEventById } = require('../controllers/events.controllers');
const { existEventById } = require('../helpers/db-content-validation');
const isDate = require('../helpers/isDate');
const validateContent = require('../middlewares/validate-content');
const { validateJwt } = require('../middlewares/validate-jwt');

const router = Router();

// middleware validate jwt para todos;
router.use(validateJwt)

// add
router.post('/',[
    check('title', 'The title is required').not().isEmpty(),
    check('start', 'The initial date is required').custom(isDate),
    check('end', 'The finish date is required').custom(isDate),
    validateContent,
], createEvent);

// updated
router.put('/:id',[
    check('id', 'Don´t MongoId').isMongoId(),
    check('id', 'Id don´t exist').custom(existEventById),
    validateContent
], updateEvent);

// delete
router.delete('/:id',[
    check('id', 'Don´t MongoId').isMongoId(),
    check('id', 'Id don´t exist').custom(existEventById),
    validateContent
], deleteEvent);

// get
router.get('/', getEvents);

// get By Id
router.get('/:id',[
    check('id', 'Don´t MongoId').isMongoId(),
    check('id', 'Id don´t exist').custom(existEventById),
    validateContent
], getEventById);


module.exports = router;