const { request, response} = require('express');
const bcryptjs = require('bcryptjs');
const UserModel = require('../models/user.model');
const { generateToken } = require('../helpers/jsonWebToken');

const createUser = async ( req = request, res = response ) => {
    try {
        const { name, email, password} = req.body;

        let user = await UserModel.findOne({ email });

        // Si el ususario existe
        if (user) {
            return res.status(400).json({
                ok: false,
                msg: 'Email and Password incorrect - email'
            });
        }


        // const body = req.body;
        user = new UserModel(req.body);
    
        // Encriptar la contraseña
        // Encrypt the password
        const salt = bcryptjs.genSaltSync(11);
        user.password = bcryptjs.hashSync(user.password, salt);
    
        // Guardar la data en la db
        // Save the data in the db
        await user.save(); 
    
         // generar JWT

         const token = await generateToken(user.id);


        res.status(201).json({
            ok: true,
            msg: 'Create User',
            user,
            token,
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error en el Servidor',
            error
        })
    }
    
}

const loginUser = async ( req = request, res = response ) => {

    try {
        const { email, password } = req.body;

        const user = await UserModel.findOne({ email });

        // Si el email existe
        // If the email exists
        if (!user) {
            return res.status(400).json({
                ok: false,
                msg: 'Email and Password incorrect - email'
            });
        }

        // Si el usuario esta activo
        // If the user is active
        // if (!user.status) {
        //     res.status(400).json({
        //         msg: 'Email and Password incorrect - status'
        //     });
        // }

        // Si la password hace macth
        const validatePass = bcryptjs.compareSync(password, user.password);

        if (!validatePass) {
            return res.status(400).json(
                {
                ok: false,
                msg: 'Email and Password incorrect - pass'
            });
        }

        // generar JWT

        const token = await generateToken(user.id);


        res.json({
            ok: true,
            msg: 'Login ok',
            user,
            token
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error en el Servidor',
            error
        });
    }
    

    
}

const renewToken = async ( req = request, res = response ) => {

    const { userAuth } = req;
    // console.log(userAuth)
    try {
        // Usuario autenticado : userAuth 
        const token = await generateToken(userAuth.id);
    
    
        res.json({
            ok: true,
            msg: 'Token Renew',
            uid: userAuth.id,
            user: userAuth.name,
            email: userAuth.email,
            token,
        })
        
    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Fail authentication'
        })
    }

}

module.exports = {
    createUser,
    loginUser,
    renewToken,
}