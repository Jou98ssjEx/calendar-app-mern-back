const { response } = require("express");
const { request } = require("express");
const { EventModel } = require("../models");

const {Types} = require('mongoose');


const createEvent = async ( req = request, res = response ) => {
    try {

        let data = req.body;
        // console.log(req)
        data.user = req.userAuth.id;
        const event = new EventModel(data);

        await event.save();

        res.json({
            ok: true,
            msg: 'Event created',
            event
        })
        
    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error,
        })
    }
}

const updateEvent = async ( req = request, res = response ) => {
    const { id } = req.params;
    const event = req.body;
    const { userAuth } = req;
    // console.log(userAuth.id)

    try {

        // Validar si la nota existe
        const foundEvent = await EventModel.findById(id);
        console.log(foundEvent.user);
        if( !foundEvent ){
            return res.status(400).json({
                ok: false,
                msg: 'Event don´t exist',
            })
        }
        // Validar si el usuario ese el que creo la nota
        if (foundEvent.user.toString() !== userAuth.id){
            return res.status(401).json({
                ok: false,
                msg: 'User don´t authorization',
            })
        }

        const eventUpdate = await EventModel.findByIdAndUpdate(id, event, {new: true});
        

        res.json({
            ok: true,
            msg: 'Event updated',
            eventUpdate,
        })
        
    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error,
        })
    }
}

const deleteEvent = async ( req = request, res = response ) => {
    const { id } = req.params;
    const { userAuth } = req;

    try {
        // validar para que solo el usuario que lo creo lo elimine
        const e = await EventModel.findById(id);
        if(!e){
            return res.status(400).json({
                ok: false,
                msg: 'Event don´t exist'
            })
        }
        // console.log(e.id)

        // console.log(userAuth.id)
        // console.log(e.user.toString())

        if( userAuth.id !== e.user.toString() ){
            return res.status(401).json({
                ok: false,
                msg: 'User don´t valid',
            });
        }

        const eventDelete = await EventModel.findByIdAndDelete(id, {new:true});
        console.log({eventDelete})
        res.json({
            ok: true,
            msg: 'Event delete',
            eventDelete,
        })
        
    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error,
        })
    }
}

const getEvents = async ( req = request, res = response ) => {

    const events = await EventModel.find().populate('user', 'name');

    try {


        res.json({
            ok: true,
            msg: 'getEvents',
            events,
        })
        
    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error,
        })
    }
}

const getEventById = async ( req = request, res = response ) => {
    const { id } = req.params;
    const eventFound = await EventModel.findById(id).populate('user', 'name');

    try {

        if (!eventFound) {
            return res.status(400).json({
                msg: `Event doesn't exist`
            })
        }
    

        res.json({
            ok: true,
            msg: 'getEventById',
            eventFound,
        })
        
    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error,
        })
    }
}



module.exports = {
    createEvent,
    updateEvent,
    deleteEvent,
    getEvents,
    getEventById,
}