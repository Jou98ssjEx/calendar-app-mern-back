const express = require('express');
const cors = require('cors');
const dbConfig = require('../database/dbConfig');

require('colors');

const url = '/api';

 class Server {
    constructor(){
        this.app = express();
        this.port = process.env.PORT;
        this.urlPath = {
            auth: `${url}/auth`,
            event: `${url}/events`,
        };
        this.midellwares();
        this.dbConnect();
        this.routes();
    }

    midellwares(){
        
        // Cors
        // Permite las peticiones de cualquier URL
        this.app.use(cors());

        // Lectura y parseo de JSON
        // El manejo de la información en formato JSON
        this.app.use(express.json());

        // Carpeta publica estatica
        this.app.use(express.static('src/public'));

    }

    async dbConnect(){

        await dbConfig();

    }

    routes(){
        this.app.use(this.urlPath.auth, require('../routers/auth.routes'));
        this.app.use(this.urlPath.event, require('../routers/events.routes'));
    }

    listener(){
        this.app.listen(this.port, () => {
            console.log(`Server online in port: ${this.port.green}`);
        })
    }
}

module.exports = Server;